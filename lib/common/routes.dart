import 'package:demo/page/car/cars.dart';
import 'package:demo/page/image_edit/mark_page_turnbox.dart';
import 'package:demo/page/image_picker/image_picker_custom.dart';
import 'package:demo/page/leads/leads.dart';
import 'package:demo/page/orders/orders.dart';
import 'package:demo/page/compression/compression.dart';
import 'package:demo/page/rate/rate.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef PageRouteCallback = Widget Function(BuildContext context);

/// 首页跳转配置类
class HomeRoutConfig {
  /// item标题
  var title = "";

  /// item颜色
  dynamic color = Colors.indigo;

  /// 页面导航
  PageRouteCallback callback;

  HomeRoutConfig(this.title, this.color, this.callback);

  @override
  String toString() {
    return 'HomeRoutConfig{title: $title, color: $color, callback: $callback}';
  }
}

List<HomeRoutConfig> _homeItems;

/// 在这里设置首页的数据
_initHomeItems() {
  if (_homeItems != null) {
    return;
  }
  _homeItems = [
    HomeRoutConfig("My Cars", Colors.indigo, (_) => CarPage()),
    HomeRoutConfig("My Leads", Colors.yellow, (_) => LeadsPage()),
    HomeRoutConfig("My Orders", Colors.green, (_) => OrdersPage()),
    HomeRoutConfig("Conversion Rate", Colors.red, (_) => RatePage()),
    HomeRoutConfig("Image Mark", Colors.white, (_) => MarkPageByTurnBox()),
    HomeRoutConfig("Image Picker", Colors.orangeAccent, (_) => ImagePicker()),
    HomeRoutConfig("Image compress", Colors.blue, (_) => CompressionContainer()),
  ];
}

List<HomeRoutConfig> getHomeItems() {
  _initHomeItems();
  return _homeItems;
}

/// 打开页面
Future<T> openPage<T extends Object>(BuildContext context, Widget page) {
  return Navigator.push(context, MaterialPageRoute(builder: (_) => page));
}

bool popBack(BuildContext context, [Object args]) {
  return Navigator.pop(context, args);
}

extension on Widget {
  open() {
  }
}
