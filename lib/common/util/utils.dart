import 'package:flutter/widgets.dart';
import 'package:flutter_luban/flutter_luban.dart';
import 'package:flutter_uploader/flutter_uploader.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

typedef OnItemClickCallback<T> = void Function(BuildContext context, T value);

class Util {

  static Future<String> compressImages(File file, String targetPath) async{
    if(targetPath==null){
      final tempDir = await getTemporaryDirectory();
      targetPath=tempDir.path;
    }
    print('######################');
    print(file.lengthSync());
    print('######################');
    CompressObject compressObject = CompressObject(
      imageFile: file, //image
      path: targetPath, //compress to path
      // mode: CompressMode.LARGE2SMALL,
      quality: 90, //first compress quality, default 80
      step:
          8, //compress quality step, The bigger the fast, Smaller is more accurate, default 6
    );

    await Luban.compressImage(compressObject).then((_path) {
      print(_path);
      targetPath=_path;
      print('********************');
      print(File(targetPath).lengthSync());
      print('********************');
    });
    return targetPath;
  }

  static Future<String> compressImagesByPath(String path, String targetPath) async{
    File file = File(path);
    return compressImages(file, targetPath);
  }

  static uploadImages(List<File> files) async{
    List<FileItem> uploadFiles = [];
    for (File file in files) {
      String filePath = await compressImages(file, null);
      uploadFiles.add(FileItem(filename: basename(filePath), savedDir: dirname(filePath)));
      print('!!!!!!!!!!!!!!!!!');
      print(uploadFiles);
      print('!!!!!!!!!!!!!!!!!');
    }
  //   final uploader = FlutterUploader();
  //   final taskId = await uploader.enqueue(
  //     url: "your upload link", //required: url to upload to
  //     files: uploadFiles, // required: list of files that you want to upload
  //     method: UploadMethod.POST, // HTTP method  (POST or PUT or PATCH)
  //     headers: {"apikey": "api_123456", "userkey": "userkey_123456"},
  //     data: {"name": "john"}, // any data you want to send in upload request
  //     showNotification: false, // send local notification (android only) for upload status
  //     tag: "image upload 1"); // unique tag for upload task
  }
}

extension on ScrollController {
  scrollToIndex(BuildContext context, double itemWidth, int index,
      {int state = 0}) {
    final windowW = MediaQuery.of(context).size.width;
    final targetLeft = itemWidth * (index - 1);
    final targetRight = itemWidth * index;
    final offset = this.offset;
    final listInWindowRight = windowW + offset;
    if (targetRight > listInWindowRight) {
      final scrollTo = targetLeft - (windowW - itemWidth);
      this.animateTo(scrollTo,
          curve: Curves.easeOut, duration: Duration(milliseconds: 300));
    }
  }
}

scrollToIndex(BuildContext context, ScrollController controller,
    double itemWidth, int index,
    {int state = 0}) {
  itemWidth ??= -1;
  if (itemWidth <= 0 || context == null || controller == null || index < 0) {
    return;
  }
  index++;
  final windowW = MediaQuery.of(context).size.width;
  final targetLeft = itemWidth * (index - 1);
  final targetRight = itemWidth * index;
  final offset = controller.offset;
  final listInWindowRight = windowW + offset;
  final listInWindowLeft = offset;
//  print("scrollToIndex -> itemWidth = $itemWidth, index = $index, "
//      "targetRight = $targetRight, listInWindowRight = $listInWindowRight");
  if (targetRight > listInWindowRight) {
    final scrollTo = targetLeft - (windowW - itemWidth);
    controller.animateTo(scrollTo,
        curve: Curves.easeOut, duration: Duration(milliseconds: 300));
  } else if (targetLeft < listInWindowLeft) {
    controller.animateTo(targetLeft,
        curve: Curves.easeOut, duration: Duration(milliseconds: 300));
  }
}
