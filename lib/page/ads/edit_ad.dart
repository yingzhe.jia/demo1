import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';

class EditAd extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new EditAdState();
  }
}

class EditAdState extends State<EditAd> {
  AssetImage pickHolder = new AssetImage('imgs/pick.png');

  TextEditingController titleController = new TextEditingController();
  TextEditingController contentController1 = new TextEditingController();
  TextEditingController contentController2 = new TextEditingController();
  TextEditingController contentController3 = new TextEditingController();

  var _image0;
  var _image1;
  var _image2;
  var _image3;
  var _image4;

  ///标题输入框
  renderTitleInput() {
    return new Padding(
        padding: new EdgeInsets.all(5.0),
        child: new TextField(
          controller: titleController,
          obscureText: false,
          decoration: InputDecoration(
              labelText: "title",
              hintText: "ads title",
              prefixIcon: Icon(Icons.lock)),
        ));
  }

  @override
  Widget build(BuildContext context) {
    var pickerWidth = MediaQuery.of(context).size.width * 0.35;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Ads',
          textAlign: TextAlign.center,
        ),
      ),
      body: ListView(children: <Widget>[
        renderTitleInput(),
        new Padding(
          padding: new EdgeInsets.only(
              left: 10.0, top: 5.0, right: 20.0, bottom: 0.0),
          child: new Row(
            children: <Widget>[
              new Text(
                'take car photos',
              ),
            ],
          ),
        ),
        new Divider(),
        Row(
          children: <Widget>[
            FlatButton(
              key: Key('upload0'),
              child: _image0 == null
                  ? new Image(image: pickHolder, width: pickerWidth)
                  : Image.file(
                      _image0,
                      width: pickerWidth,
                    ),
              onPressed: _pick0,
            ),
            Expanded(
              child: new Container(),
            ),
            FlatButton(
              key: Key('upload1'),
              child: _image1 == null
                  ? new Image(image: pickHolder, width: pickerWidth)
                  : Image.file(
                      _image1,
                      width: pickerWidth,
                    ),
              onPressed: _pick1,
            ),
          ],
        ),
        Row(children: <Widget>[
          new Padding(
              padding: new EdgeInsets.only(
                  left: 0.0, top: 10.0, right: 0.0, bottom: 0.0),
              child: Container()),
        ]),
        Row(
          children: <Widget>[
            new Padding(
                padding: new EdgeInsets.only(
                    left: 30.0, top: 0.0, right: 20.0, bottom: 0.0),
                child: Container()),
            new Text('front'),
            Expanded(
              child: new Container(),
            ),
            new Text('back'),
            new Padding(
                padding: new EdgeInsets.only(
                    left: 30.0, top: 0.0, right: 20.0, bottom: 0.0),
                child: Container()),
          ],
        ),
        Row(children: <Widget>[
          new Padding(
              padding: new EdgeInsets.only(
                  left: 0.0, top: 20.0, right: 0.0, bottom: 0.0),
              child: Container()),
        ]),
        Row(
          children: <Widget>[
            FlatButton(
              child: _image2 == null
                  ? new Image(image: pickHolder, width: pickerWidth)
                  : Image.file(
                      _image2,
                      width: pickerWidth,
                    ),
              onPressed: _pick2,
            ),
            Expanded(
              child: new Container(),
            ),
            FlatButton(
              child: _image3 == null
                  ? new Image(image: pickHolder, width: pickerWidth)
                  : Image.file(
                      _image3,
                      width: pickerWidth,
                    ),
              onPressed: _pick3,
            ),
          ],
        ),
        Row(children: <Widget>[
          new Padding(
              padding: new EdgeInsets.only(
                  left: 0.0, top: 10.0, right: 0.0, bottom: 0.0),
              child: Container()),
        ]),
        Row(
          children: <Widget>[
            new Padding(
                padding: new EdgeInsets.only(
                    left: 30.0, top: 0.0, right: 20.0, bottom: 0.0),
                child: Container()),
            new Text('left'),
            Expanded(
              child: new Container(),
            ),
            new Text('right'),
            new Padding(
                padding: new EdgeInsets.only(
                    left: 30.0, top: 0.0, right: 20.0, bottom: 0.0),
                child: Container()),
          ],
        ),
        Row(children: <Widget>[
          new Padding(
              padding: new EdgeInsets.only(
                  left: 0.0, top: 20.0, right: 0.0, bottom: 0.0),
              child: Container()),
        ]),
        Row(
          children: <Widget>[
            FlatButton(
              child: _image4 == null
                  ? new Image(image: pickHolder, width: pickerWidth)
                  : Image.file(
                      _image4,
                      width: pickerWidth,
                    ),
              onPressed: _pick4,
            ),
            Expanded(
              child: new Container(),
            ),
          ],
        ),
        Row(children: <Widget>[
          new Padding(
              padding: new EdgeInsets.only(
                  left: 0.0, top: 10.0, right: 0.0, bottom: 0.0),
              child: Container()),
        ]),
        Row(
          children: <Widget>[
            new Padding(
                padding: new EdgeInsets.only(
                    left: 30.0, top: 0.0, right: 20.0, bottom: 0.0),
                child: Container()),
            new Text('45 degree'),
            Expanded(
              child: new Container(),
            ),
          ],
        ),

        TextField(
          controller: contentController1,
          obscureText: false,
          decoration: InputDecoration(
              labelText: "item1",
              hintText: "item1 content",
              prefixIcon: Icon(Icons.input)),
        ),

        TextField(
          controller: contentController2,
          obscureText: false,
          decoration: InputDecoration(
              labelText: "item2",
              hintText: "item2 content",
              prefixIcon: Icon(Icons.input)),
        ),

        TextField(
            maxLines: 2,
            controller: contentController3,
            decoration: new InputDecoration.collapsed(
              hintText: 'Content',
            ),
            // style: GSYConstant.middleText,
          ),
        
        Row(children: <Widget>[
          new Padding(
              padding: new EdgeInsets.only(
                  left: 0.0, top: 10.0, right: 0.0, bottom: 0.0),
              child: Container()),
        ]),
        Row(
          children: <Widget>[
            Expanded(
                child: FlatButton(
              child: Text('preview',
                  style: new TextStyle(color: Colors.white, fontSize: 18)),
              onPressed: _submitFileUpload,
              color: Colors.red,
            )),
            Expanded(
                child: FlatButton(
              child: Text('save',
                  style: new TextStyle(color: Colors.white, fontSize: 18)),
              onPressed: _submitFileUpload,
              color: Colors.red,
            )),
            Expanded(
                child: FlatButton(
              child: Text('launch',
                  style: new TextStyle(color: Colors.white, fontSize: 18)),
              onPressed: _submitFileUpload,
              color: Colors.red,
            )),
          ],
        ),
        new Padding(
            padding: new EdgeInsets.only(
                left: 0.0, top: 10.0, right: 0.0, bottom: 0.0),
            child: Container()),
      ]),
    );
  }

  _pick0() async {
    _pick(0);
  }

  _pick1() async {
    _pick(1);
  }

  _pick2() async {
    _pick(2);
  }

  _pick3() async {
    _pick(3);
  }

  _pick4() async {
    _pick(4);
  }

  _pick(int type) async {
    print('pick' + type.toString());
    var result = showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('where do u wanna get photo?'),
            actions: <Widget>[
              FlatButton(
                child: const Text('album'),
                onPressed: () {
                  Navigator.of(context).pop(0);
                },
              ),
              FlatButton(
                  child: const Text('camera'),
                  onPressed: () {
                    Navigator.of(context).pop(1);
                  }),
            ],
          );
        });
    result.then((v) {
      ImageSource src = v == 0 ? ImageSource.gallery : ImageSource.camera;
      _pickImage(type, src);
    });
  }

  _pickImage(int type, ImageSource src) async {
    try {
      var imgTmp = await ImagePicker.pickImage(source: src);
      switch (type) {
        case 0:
          this._image0 = imgTmp;
          break;
        case 1:
          this._image1 = imgTmp;
          break;
        case 2:
          this._image2 = imgTmp;
          break;
        case 3:
          this._image3 = imgTmp;
          break;
        case 4:
          this._image4 = imgTmp;
          break;
      }
      setState(() {});
    } catch (e) {
      _toast(e);
    }
  }

  _toast(info) {
    Fluttertoast.showToast(
        msg: info,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.redAccent,
        textColor: Colors.white,
        fontSize: 18.0);
  }

  _submitFileUpload() async {
    print('upload');
    if (_image0 == null ||
        _image1 == null ||
        _image2 == null ||
        _image3 == null ||
        _image4 == null) {
      _toast('please upload images');
      return;
    }
    //todo call api
    Navigator.of(context).pop(1);
  }
}
