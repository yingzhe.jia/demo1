import 'package:flutter/material.dart';

import 'car_list.dart';

class CarPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new CarPageState();
  }
}

class CarPageState extends State<CarPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController; //需要定义一个Controller
  List tabs = ["selling", "selled"];

  @override
  void initState() {
    super.initState();
    // 创建Controller
    _tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //导航栏
        title: Text("My Cars"),
        // actions: <Widget>[
        //   //导航栏右侧菜单
        //   IconButton(icon: Icon(Icons.share), onPressed: () {}),
        // ],
        bottom: TabBar(
            //生成Tab菜单
            controller: _tabController,
            tabs: tabs.map((e) => Tab(text: e)).toList()),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[CarListPage(1), CarListPage(2)],
      ),
    );
  }
}