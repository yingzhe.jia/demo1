import 'dart:typed_data';

import 'package:demo/fonts/demo_icon_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';

import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:ui' as ui;

class MarkPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MarkState();
}

class MarkState extends State<MarkPage> with SingleTickerProviderStateMixin {

  @override
  void initState() {
    super.initState();
    PermissionHandler().requestPermissions(<PermissionGroup>[
      PermissionGroup.storage, 
    ]);
    for (int i = 1; i <= 100; i ++) {
      availableKeyList.add(i);
    }
  }

  List<int> availableKeyList = [];

  final GlobalKey _repaintKey = new GlobalKey();

  Map<int, MyPosition> positionMap = {};

  int currMax = 1;

  int currKey;

  var _img;

  double _scale = 1.0;
  double _previousScale;
  var yOffset = 400.0;
  var xOffset = 50.0;
  var rotation = 0.0;
  var lastRotation = 0.0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("image editor demo"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.photo_library),
              onPressed: _pick,
            ),
            IconButton(
              icon: Icon(Icons.done),
              onPressed: () async {
                RenderRepaintBoundary boundary =
                    _repaintKey.currentContext.findRenderObject();
                ui.Image image = await boundary.toImage();
                ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
                final result = await ImageGallerySaver.saveImage(byteData.buffer.asUint8List());
                print(result);
                _toast('save success!');
              },
            ),
          ],
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: Center(
                child: RepaintBoundary(
                  key: _repaintKey,
                  child: Stack(
                      alignment: Alignment.topCenter,
                      children: _getWidgetList()
                      ),
                ),
              ),
            ),
            Container(
              height: 60.0,
              child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: <Widget>[
                  Text('size'),
                  Slider(
                      value: currKey == null ? 40 : positionMap[currKey].size,
                      min: 30,
                      max: 300,
                      label: 'Width',
                      onChanged: (e) {
                        if (positionMap[currKey] != null) {
                          setState(() {
                            positionMap[currKey].size = e.roundToDouble();
                          });
                        }
                      }),
                ],
              ),
            ),
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          color: Colors.lightBlue,
          shape: CircularNotchedRectangle(),
          child: ButtonTheme(
            minWidth: 0.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                FlatButton(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.add),
                      const SizedBox(height: 5.0),
                      Text(
                        "add",
                        style: TextStyle(fontSize: 8.0),
                      ),
                    ],
                  ),
                  textColor: Colors.white,
                  onPressed: () {
                    int key = availableKeyList.first;
                    availableKeyList.removeAt(0);
                    positionMap[key] = MyPosition(20, 20, 40, 0, false);
                    setState(() {});
                  },
                ),
                FlatButton(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.remove),
                      const SizedBox(height: 5.0),
                      Text(
                        "remove",
                        style: TextStyle(fontSize: 8.0),
                      ),
                    ],
                  ),
                  textColor: Colors.white,
                  onPressed: () {
                    if (currKey == null) {
                      return;
                    }
                    availableKeyList.add(currKey);
                    positionMap[currKey] = null;
                    currKey = null;
                    setState(() {});
                  },
                ),
                FlatButton(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.flip),
                      const SizedBox(height: 5.0),
                      Text(
                        "Flip",
                        style: TextStyle(fontSize: 8.0),
                      ),
                    ],
                  ),
                  textColor: Colors.white,
                  onPressed: () {
                    if (currKey == null) {
                      return;
                    }
                    setState(() {
                      positionMap[currKey].isFlip = !positionMap[currKey].isFlip;
                    });
                  },
                ),
                FlatButton(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.rotate_left),
                      const SizedBox(height: 5.0),
                      Text(
                        "Rotate Left",
                        style: TextStyle(fontSize: 8.0),
                      ),
                    ],
                  ),
                  textColor: Colors.white,
                  onPressed: () {
                    if (currKey == null) {
                      return;
                    }
                    setState(() {
                      positionMap[currKey].quarterTurns += 3;
                      positionMap[currKey].quarterTurns %= 4;
                    });
                  },
                ),
                FlatButton(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.rotate_right),
                      const SizedBox(height: 5.0),
                      Text(
                        "Rotate Right",
                        style: TextStyle(fontSize: 8.0),
                      ),
                    ],
                  ),
                  textColor: Colors.white,
                  onPressed: () {
                    if (currKey == null) return;
                    setState(() {
                      positionMap[currKey].quarterTurns += 1;
                      positionMap[currKey].quarterTurns %= 4;
                    });
                  },
                ),
                FlatButton(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.restore),
                      const SizedBox(height: 5.0),
                      Text(
                        "Reset",
                        style: TextStyle(fontSize: 8.0),
                      ),
                    ],
                  ),
                  textColor: Colors.white,
                  onPressed: () {
                    if (currKey == null) return;
                    setState(() {
                      positionMap[currKey].quarterTurns = 0;
                      positionMap[currKey].size = 40;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _getWidgetList() {
    List<Widget> list = [];
    if (_img == null) {
      list.add(Container());
    } else {
      list.add(Image.file(
        _img,
        fit: BoxFit.cover,
      ));
    }
    for (int i = 1; i <= 100; i ++) {
      list.add(this._generatePositionedWidget(i));
    }
    return list;
  }

  Widget _generatePositionedWidget(int key) {
    if (positionMap == null || positionMap[key] == null) {
      return Container();
    } else {
      return Positioned(
        top: positionMap[key].top,
        left: positionMap[key].left,
        child: GestureDetector(
            child: RotatedBox(
              quarterTurns: positionMap[key].quarterTurns,
              child: Icon(
                // Icons.live_help,
                DemoIcon.demo,
                size: positionMap[key].size,
                color: Colors.blueGrey,
                textDirection: positionMap[key].isFlip ? TextDirection.rtl : TextDirection.ltr,
              ),
            ),
            onPanDown: (DragDownDetails e) {
              this.currKey = key;
            },
            onPanUpdate: (DragUpdateDetails e) {
              setState(() {
                positionMap[key].left += e.delta.dx;
                positionMap[key].top += e.delta.dy;
              });
            },
            onPanEnd: (DragEndDetails e) {
            },
            onScaleStart: (scaleDetails) {
                  _previousScale = _scale;
                  print(' scaleStarts = ${scaleDetails.focalPoint}');
            },
            ),
      );
    }
  }

  _pick() async {
    var result = showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('where do u wanna get photo?'),
            actions: <Widget>[
              FlatButton(
                child: const Text('gallery'),
                onPressed: () {
                  Navigator.of(context).pop(0);
                },
              ),
              FlatButton(
                  child: const Text('camera'),
                  onPressed: () {
                    Navigator.of(context).pop(1);
                  }),
            ],
          );
        });
    result.then((v) {
      ImageSource src = v == 0 ? ImageSource.gallery : ImageSource.camera;
      _pickImage(0, src);
    });
  }

  _pickImage(int type, ImageSource src) async {
    try {
      var imgTmp = await ImagePicker.pickImage(source: src);
      _img = imgTmp;
      setState(() {});
    } catch (e) {
      _toast(e);
    }
  }

  _toast(info) {
    Fluttertoast.showToast(
        msg: info,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.redAccent,
        textColor: Colors.white,
        fontSize: 18.0);
  }
}

class MyPosition {
  double top = 20.0;
  double left = 20.0;
  double size = 40;
  int quarterTurns = 0;
  bool isFlip = false;
  MyPosition(this.top, this.left, this.size, this.quarterTurns, this.isFlip);
}
