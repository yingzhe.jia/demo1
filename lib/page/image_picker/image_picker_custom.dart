import 'dart:ui';

import 'package:demo/common/routes.dart';
import 'package:demo/common/util/utils.dart';
import 'package:demo/page/image_edit/mark_page_turnbox.dart';
import 'package:demo/page/image_picker/image_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:photo_manager/photo_manager.dart';

import 'image_picker_toolbox.dart';

class ImagePicker extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ImagePickerState();
  }
}

class _ImagePickerState extends State<ImagePicker> {
  List<ImagePickerItemModel> mDataImages;

  List<ImagePickerSlotModel> mData = getTestImageSlot();
  ImagePickerSlotModel currModel;

  ScrollController controller = ScrollController();

  _initTools() async {
    await PhotoManager.requestPermission();
    var data = await PhotoManager.getAssetPathList(type: RequestType.image);

    for (var item in data) {
      if (item.id != "isAll") continue;
      var imgs = await item.getAssetListPaged(0, 1000);
      mDataImages = imgs.map((e) => ImagePickerItemModel(e)).toList();
      break;
    }
    print("-------------------------");
    print(mDataImages);
    print("-------------------------");
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _initTools();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Image Picker'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                openPage(context, MarkPageByTurnBox());
              }),
        ],
      ),
      body: Container(
        color: Colors.black87,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            /// 头部文字
            Padding(
              padding: EdgeInsets.only(left: 10, top: 15, bottom: 10),
              child: Text(
                "Optional Document/Item",
                style: TextStyle(color: Colors.white),
              ),
            ),

            /// 头部图片导航的布局
            Container(
              constraints: BoxConstraints.expand(height: 90),
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  controller: controller,
                  itemCount: mData.length,
                  itemBuilder: (context, index) {
                    if (mData[index].isSelected) currModel = mData[index];
                    return _NaviImageItemView(mData[index], (context, model) {
                      _gotoItem(model);
                    });
                  }),
            ),
            Expanded(
                child: mDataImages == null
                    ? Container(
                        child: Center(
                          child: Container(
                            width: 20,
                            height: 20,
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                            ),
                          ),
                        ),
                      )

                    /// 图片列表的UI
                    : GridView.builder(
                        itemCount: mDataImages.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3),
                        itemBuilder: (context, index) {
                          return _ImageItemView(mDataImages[index],
                              (context, model) {
                            if (currModel == null) return;

                            var refreshModel = currModel;

                            if (model.isSelected && model.slot != null) {
                              refreshModel = model.slot;
                            }

                            final max = refreshModel.max;

                            var items = refreshModel.imageItems;

                            // 要刷新的坑位就是当前坑位，
                            // 并且当前已经是最大值了，并且当前model状态是非选中
                            // 则需要跳转选框到下一个坑位，并且刷新坑位
                            if (refreshModel == currModel &&
                                items.length == max &&
                                !model.isSelected) {
                              final result = _gotoNext();
                              if (!result) {
                                // 此时说明所有都已经选完
                                return;
                              }
                              refreshModel = currModel;
                              items = refreshModel.imageItems;
                            }

                            model.isSelected = !model.isSelected;

                            final isEmpty = items.isEmpty;

                            if (model.isSelected) {
                              items.add(model);
                              model.slot = refreshModel;
                            } else {
                              items.remove(model);
                              model.slot = null;
                            }
                            setState(() {});
//                              if ((items.isEmpty && !isEmpty) ||
//                                  (items.isNotEmpty && isEmpty)) {
//                                setState(() {});
//                              }
                          });
                        })),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
    disposeCache();
    _NaviImageItemView.itemWidth = -1.0;
  }

  bool _gotoNext() {
    var index = mData.indexOf(currModel);

    var oldIndex = index;

    while (true) {
      index++;
      if (index >= mData.length) {
        if (oldIndex > 0) {
          oldIndex = -1;
          index = -1;
          continue;
        }
        return false;
      }
      if (mData[index].imageItems.length >= mData[index].max) {
        continue;
      }
      break;
    }

    _gotoItem(mData[index]);

    return true;
  }

  _gotoItem(ImagePickerSlotModel model) {
    var index = mData.indexOf(model);

    scrollToIndex(context, controller, _NaviImageItemView.itemWidth, index);

    if (model.isSelected) return;
    mData.forEach((item) {
      item.isSelected = false;
    });
    model.isSelected = true;
    currModel = model;
    setState(() {});
  }
}

/// 头部导航条的Item样式
class _NaviImageItemView extends StatelessWidget {
  ImagePickerSlotModel model;

  OnItemClickCallback<ImagePickerSlotModel> callback;

  _NaviImageItemView(this.model, this.callback);

  static var itemWidth = -1.0;

  @override
  Widget build(BuildContext context) {
    /// 图片控件
    var img = Card(
      color: Colors.white30,
      shape: Border.all(
        color: model.isSelected ? Colors.yellow[300] : Colors.black87,
      ),
      child: InkWell(
        onTap: () => callback(context, model),
        child: SizedBox.fromSize(
          child: getNaviImage(model),
          size: Size(80, 60),
        ),
      ),
    );

    /// 图片下的文本
    final items = model.imageItems;
    var text = Text("${model.title} (${items.length}/${model.max})",
        style: TextStyle(color: Colors.white, fontSize: 9));

    if (itemWidth <= 0) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        itemWidth = context.size.width;
      });
    }

    return Column(
      children: <Widget>[img, text],
    );
  }
}

/// 图片列表的Item样式
class _ImageItemView extends StatefulWidget {
  ImagePickerItemModel model;

  OnItemClickCallback<ImagePickerItemModel> callback;

  _ImageItemView(this.model, this.callback);

  @override
  State<StatefulWidget> createState() {
    return _ImageItemViewState(model, callback);
  }
}

class _ImageItemViewState extends State<_ImageItemView> {
  ImagePickerItemModel model;

  OnItemClickCallback<ImagePickerItemModel> callback;

  _ImageItemViewState(this.model, this.callback);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border.all(color: Colors.white)),
      child: Stack(
        children: <Widget>[
          InkWell(
            onTap: () async {
              if (model.file == null) {
                model.file = await model.imageEntity.file;
              }
              openPage(context, ImagePreview(model.file));
            },
            child: ConstrainedBox(
              constraints: BoxConstraints.expand(),
              child: getGridImage(model),
            ),
          ),
          Positioned(
              top: -13,
              right: -13,
              child: InkWell(
                  onTap: () {
                    callback(context, model);
                    setState(() {});
                  },
                  child: IgnorePointer(
                    child: Radio(
                        value: model.isSelected,
                        groupValue: true,
                        onChanged: (value) {}),
                  )))
        ],
      ),
    );
  }
}
