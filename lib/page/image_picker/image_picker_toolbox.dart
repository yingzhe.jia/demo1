import 'dart:collection';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';

/// 图片选择的坑位Model
class ImagePickerSlotModel with Selectable {
  /// 坑位名称
  String title;

  /// 图片路径，如果是多图，则此属性为第一张图的路径
  String imagePath;

  /// 图片路径，如果是单图，则此属性为null
  List<String> imagePaths;

  /// 存放img列表，用于最后的计算，外部不要使用
  List<ImagePickerItemModel> imageItems = [];

  /// 最大张数
  int _max = 0;

  /// 最小张数
  int min = 0;

  /// 是否为必选
  bool isRequired;

  int get max {
    return _max > 0 ? _max : 1;
  }

  ImagePickerSlotModel(this.title,
      {int max, this.min, this.isRequired, this.imagePath, this.imagePaths})
      : _max = max;
}

class ImagePickerItemModel with Selectable {
  AssetEntity imageEntity;
  File file;
  Uint8List data;
  ImagePickerSlotModel slot;

  ImagePickerItemModel(this.imageEntity);
}

final Queue<ImagePickerItemModel> _imgCaches = ListQueue();
const _maxCacheSize = 24;

/// 获取图片导航的Item控件
Widget getNaviImage(ImagePickerSlotModel model) {
  if (model.imageItems.isEmpty) {
    return Icon(
      Icons.add,
      size: 20,
      color: Colors.white,
    );
  }
  final item = model.imageItems[0];

  if (_imgCaches.contains(item)) {
    _imgCaches.remove(item);
    if (item.data == null) {
      return _getFutureImage(item);
    }
    _imgCaches.addFirst(item);
    return Image.memory(
      item.data,
      fit: BoxFit.cover,
    );
  } else {
    return _getFutureImage(item);
  }
}

/// 获取图片列表的Item控件
Widget getGridImage(ImagePickerItemModel model) {
  if (_imgCaches.contains(model)) {
    _imgCaches.remove(model);
    if (model.data == null) {
      return _getFutureImage(model);
    }
    _imgCaches.addFirst(model);
    return Image.memory(
      model.data,
      fit: BoxFit.cover,
    );
  } else {
    return _getFutureImage(model);
  }
}

void disposeCache() {
  _imgCaches.forEach((e) => e.data = null);
}

/// 获取图片控件
Widget _getFutureImage(ImagePickerItemModel model) {
  return FutureBuilder(
    future: model.imageEntity.thumbData,
    builder: (BuildContext context, AsyncSnapshot<Uint8List> snapshot) {
      final isEmpty = snapshot.data == null;
      if (!isEmpty) {
        model.data = snapshot.data;
        _imgCaches.addFirst(model);

        _resize();
      }
      return isEmpty
          ? SizedBox()
          : Image.memory(
              snapshot.data,
              fit: BoxFit.cover,
            );
    },
  );
}

void _resize() {
  var overflow = _imgCaches.length - _maxCacheSize;
  while (overflow > 0) {
    final last = _imgCaches.removeLast();
    last.data = null;
    overflow--;
  }
}

mixin Selectable {
  /// 当前是否被选中
  bool isSelected = false;
}

getTestImageSlot() {
  return [
    ImagePickerSlotModel(
      "*RC back",
      max: 3,
      isRequired: true,
    )..isSelected = true,
    ImagePickerSlotModel(
      "*RC front",
      max: 1,
      isRequired: true,
    ),
    ImagePickerSlotModel(
      "*RC front2",
      max: 2,
      isRequired: true,
    ),
    ImagePickerSlotModel(
      "*RC front3",
      max: 3,
      isRequired: true,
    ),
    ImagePickerSlotModel(
      "*RC front4",
      max: 3,
      isRequired: true,
    ),
    ImagePickerSlotModel(
      "*RC front5",
      max: 3,
      isRequired: true,
    ),
    ImagePickerSlotModel(
      "*RC front6",
      max: 3,
      isRequired: true,
    ),
    ImagePickerSlotModel(
      "*RC front7",
      max: 3,
      isRequired: true,
    ),
  ];
}
