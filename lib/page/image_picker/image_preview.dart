import 'dart:io';

import 'package:demo/common/routes.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

/// 图片预览页面，暂时只支持file入参，后续根据需要在扩展入参
class ImagePreview extends StatefulWidget {
  final File file;

  ImagePreview(this.file);

  @override
  State<StatefulWidget> createState() {
    return _ImagePreviewState();
  }
}

class _ImagePreviewState extends State<ImagePreview> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Image Preview'),
        leading: Builder(builder: (context) {
          return IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white), //自定义图标
            onPressed: () {
              popBack(context);
            },
          );
        }),
      ),
      body: Container(
        color: Colors.black87,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: PhotoView.customChild(
          maxScale: 2.0,
          minScale: 1.0,
          child: Image.file(
            widget.file,
            frameBuilder: (
              BuildContext context,
              Widget child,
              int frame,
              bool wasSynchronouslyLoaded,
            ) {
              print(
                  "wasSynchronouslyLoaded = $wasSynchronouslyLoaded, frame = $frame");
              if (wasSynchronouslyLoaded) {
                return child;
              }
              return AnimatedOpacity(
                child: child,
                opacity: frame == null ? 0 : 1,
                duration: const Duration(milliseconds: 300),
                curve: Curves.easeOut,
              );
            },
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
