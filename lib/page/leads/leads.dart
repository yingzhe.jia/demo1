import 'package:flutter/material.dart';
import 'lead_list.dart';

class LeadsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LeadsPageState();
  }
}

class LeadsPageState extends State<LeadsPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController; //需要定义一个Controller
  List tabs = ["new", "tracking", 'dealed', 'invalid'];

  @override
  void initState() {
    super.initState();
    // 创建Controller
    _tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //导航栏
        title: Text("My Leads"),
        // actions: <Widget>[
        //   //导航栏右侧菜单
        //   IconButton(icon: Icon(Icons.share), onPressed: () {}),
        // ],
        bottom: TabBar(
            //生成Tab菜单
            controller: _tabController,
            tabs: tabs.map((e) => Tab(text: e)).toList()),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[LeadsListPage(1), LeadsListPage(2), LeadsListPage(3), LeadsListPage(4)],
      ),
    );
  }
}