import 'package:demo/page/home.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new LoginPageState();
  }
  
}

class LoginPageState extends State<LoginPage> {

  final TextEditingController userController = new TextEditingController();

  final TextEditingController pwController = new TextEditingController();

  var _userName = "";

  var _password = "";
  @override
  Widget build(BuildContext context) {
    return Container(
          color: Theme.of(context).primaryColor,
          child: new Center(
            ///防止overFlow的现象
            child: SafeArea(
              ///同时弹出键盘不遮挡
              child: SingleChildScrollView(
                child: new Card(
                  elevation: 5.0,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  color: Colors.white,
                  margin: const EdgeInsets.only(left: 30.0, right: 30.0),
                  child: new Padding(
                    padding: new EdgeInsets.only(
                        left: 30.0, top: 40.0, right: 30.0, bottom: 0.0),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Padding(padding: new EdgeInsets.all(10.0)),
                        TextField(
                          // autofocus: true,
                          decoration: InputDecoration(
                              labelText: "username",
                              hintText: "ur username",
                              prefixIcon: Icon(Icons.person)
                          ),
                          controller: userController,
                        ),
                        new Padding(padding: new EdgeInsets.all(10.0)),
                        TextField(
                          decoration: InputDecoration(
                              labelText: "password",
                              hintText: "ur password",
                              prefixIcon: Icon(Icons.lock)
                          ),
                          controller: pwController,
                          obscureText: true,
                        ),
                        new Padding(padding: new EdgeInsets.all(30.0)),
                        new FlatButton(
                          child: Text('LogIn'),
                          color: Theme.of(context).primaryColor,
                          textColor: Colors.white,
                          onPressed: loginIn,
                        ),
                        new Padding(padding: new EdgeInsets.all(15.0)),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
  }

  loginIn() async {
    _userName = userController.text;
    _password = pwController.text;
    if (_userName == null || _userName.isEmpty) {
      return;
    }
    if (_password == null || _password.isEmpty) {
      return;
    }

    Navigator.push(context, MaterialPageRoute(builder: (context) {return HomePage();}));
  }
  
}