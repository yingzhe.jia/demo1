import 'dart:math';
import 'package:flutter/material.dart';

class OrdersListPage extends StatefulWidget {
  final int type;

  const OrdersListPage(this.type);

  @override
  State<StatefulWidget> createState() {
    return OrdersListPageState(type);
  }
  
}

class OrdersListPageState extends State<OrdersListPage> {
  final int type;
  static const loadingTag = "##loading##"; //表尾标记
  var _words = <String>[loadingTag];

  var _cars = <String>['BMW', 'BENZ', 'TOYOTA', 'HONDA', 'OTHER'];

  OrdersListPageState(this.type);

  @override
  void initState() {
    super.initState();
    _retrieveData();
  }

  void _retrieveData() {
    Future.delayed(Duration(seconds: 1)).then((e) {
      Random ran = Random();
      ran.nextInt(_cars.length);
      for (int i = 0; i < 20; i++) {
        _words.insert(_words.length - 1, _cars[ran.nextInt(_cars.length)]);
      }
      // _words.insertAll(_words.length - 1,
      //     //每次生成20个单词
      //     generateWordPairs().take(20).map((e) => e.asPascalCase).toList()
      // );
      setState(() {
        //重新构建列表
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: _words.length,
      itemBuilder: (context, index) {
        //如果到了表尾
        if (_words[index] == loadingTag) {
          //不足100条，继续获取数据
          if (_words.length - 1 < 40) {
            //获取数据
            _retrieveData();
            //加载时显示loading
            return Container(
              padding: const EdgeInsets.all(16.0),
              alignment: Alignment.center,
              child: SizedBox(
                  width: 24.0,
                  height: 24.0,
                  child: CircularProgressIndicator(strokeWidth: 2.0)
              ),
            );
          } else {
            //已经加载了100条数据，不再获取数据。
            return Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(16.0),
                child: Text("no more", style: TextStyle(color: Colors.grey),)
            );
          }
        }
        //显示单词列表项
        // return ListTile(title: Text(_words[index]));
        return Card(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.airport_shuttle),
                  Text('      car info'),
                ]
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.people),
                  Text('      customer name'),
                ]
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.attach_money),
                  Text('      100000'),
                ]
              ),
            ]
          ),
        );
      },
      separatorBuilder: (context, index) => Divider(height: .0),
    );
  }
}