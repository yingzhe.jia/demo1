import 'package:flutter/material.dart';
import 'order_list.dart';

class OrdersPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new OrdersPageState();
  }
}

class OrdersPageState extends State<OrdersPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController; //需要定义一个Controller
  List tabs = ["processing", 'dealed', 'refund'];

  @override
  void initState() {
    super.initState();
    // 创建Controller
    _tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //导航栏
        title: Text("My Orders"),
        // actions: <Widget>[
        //   //导航栏右侧菜单
        //   IconButton(icon: Icon(Icons.share), onPressed: () {}),
        // ],
        bottom: TabBar(
            //生成Tab菜单
            controller: _tabController,
            tabs: tabs.map((e) => Tab(text: e)).toList()),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[OrdersListPage(1), OrdersListPage(2), OrdersListPage(3)],
      ),
    );
  }
}